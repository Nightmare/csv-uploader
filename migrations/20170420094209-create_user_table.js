'use strict';

module.exports = {
    up: function (queryInterface, dataTypes) {
        return queryInterface.createTable('users', {
            id: {
                primaryKey: true,
                type: dataTypes.UUID,
                defaultValue: dataTypes.UUIDV4,
            },
            email: {
                type: dataTypes.STRING,
                allowNull: false,
            },
            first_name: {
                type: dataTypes.STRING,
                allowNull: false,
            },
            surname: {
                type: dataTypes.STRING,
                allowNull: false,
            },
        }).then(() => queryInterface.addIndex(
            'users',
            ['email'],
            {
                indexName: 'idx-users-email',
                indicesType: 'UNIQUE'
            }
        ));
    },

    down: function (queryInterface) {
        return queryInterface.dropTable('users');
    },
};
