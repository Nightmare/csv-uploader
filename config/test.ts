'use strict';

import * as path from 'path';

export = {
    sequelize: {
        storage: path.join(__dirname, '..', '..', 'test', 'storages') + '/test.db',
    },
};
