'use strict';

import * as process from 'process';
import * as path from 'path';

const ENV: string = process.env.NODE_ENV;

export = {
    env: ENV,
    is_prod: ENV === 'production',
    port: process.env.PORT || 8081,
    public_path: path.join(__dirname, '..', '..', 'public'),
    views_path: path.join(__dirname, '..', '..', 'views'),
    sequelize: {
        logging: false,
        dialect: 'sqlite',
        storage: path.join(__dirname, '..', '..', 'storages') + '/database.db',
    },
};
