'use strict';

import * as process from 'process';
import * as config from 'config';

/**
 * This file for sequelize migrations only
 *
 * @type {{}}
 */
export = {
    [process.env.NODE_ENV || 'development']: config.get('sequelize')
};