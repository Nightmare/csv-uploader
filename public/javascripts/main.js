$(document).ready(function () {
    $('#upload-form').submit(e => {
        e.preventDefault();
        const $this = $(this);
        $.ajax({
            type: $this.attr('method'),
            url: $this.attr('action'),
            data: {},
            xhrFields: {
                onprogress: function (e) {
                    console.log(e);
                    if (e.lengthComputable) {
                        console.log(e.loaded / e.total * 100 + '%');
                    }
                }
            },
            success: (data) => {
                //Do something success-ish
            }
        });
    });
});
