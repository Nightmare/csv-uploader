'use strict';

/**
 * Replace global Promise object to Bluebird
 */
import * as Bluebird from 'bluebird';
global.Promise = Bluebird as any;

/**
 * Automatically catch all errors
 */
import './mv/express-promise';

import * as http from 'http';
import * as helmet from 'helmet';
import * as config from 'config';
import * as express from 'express';
import * as compression from 'compression';
import {json, urlencoded} from 'body-parser';
import {HttpError, NotFoundHttpError} from './errors';
import {router as CSVRouter} from './routes/csv';

class AppServer {
    private static instance: AppServer;
    public app: express.Application;
    public server: http.Server;

    /**
     * Entry point (FrontController + Singleton)
     *
     * @returns {AppServer}
     */
    public static getInstance(): AppServer {
        if (!AppServer.instance) {
            AppServer.instance = new AppServer();
        }
        return AppServer.instance;
    }

    private constructor() {
        this.app = express();
        this.config();
        this.routes();
        this.notFound();
        this.error();

        this.server = http.createServer(this.app);
        this.serverListen();
    }

    private config(): this {
        this.app.disable('x-powered-by');

        this.app.set('etag', false);
        this.app.set('views', config.get('views_path') as string);
        this.app.set('view engine', 'jade');

        if (config.get('is_prod')) {
            this.app.enable('trust proxy');
        }

        this.app.use(helmet());
        this.app.use(json());
        this.app.use(urlencoded({extended: false}));
        this.app.use(compression());
        this.app.use(express.static(config.get('public_path') as string));

        return this;
    }

    private routes(): this {
        this.app.get('/', (req: express.Request, res: express.Response) => res.render('index'));
        this.app.use('/csv', CSVRouter);

        return this;
    }

    private notFound(): this {
        this.app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
            throw new NotFoundHttpError('Not found', 'not_found');
        });

        return this;
    }

    private error(): this {
        this.app.use((err: any | Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
            const {errStatus, errRes} = this.processError(err);

            if (!res.headersSent) {
                res.status(errStatus).json(errRes);
            } else {
                res.end();
            }
        });

        return this;
    }

    private serverListen(): this {
        this.server.on('error', (error: any) => {
            if (error.syscall !== 'listen') {
                throw error;
            }

            const addr = this.server.address();
            const bind = typeof addr === 'string' ? 'Pipe ' + addr : 'Port ' + addr.port;

            switch (error.code) {
                case 'EACCES':
                    console.error(bind + ' requires elevated privileges');
                    process.exit(1);
                    break;

                case 'EADDRINUSE':
                    console.error(bind + ' is already in use');
                    process.exit(1);
                    break;

                default:
                    throw error;
            }
        });

        this.server.on('listening', () => {
            const addr = this.server.address();
            console.info('Listening on', typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port);
        });
        this.server.listen(config.get('port'));

        return this;
    }

    private processError(err: Error | any): { errStatus: any, errRes: any } {
        const isDebug = config.get('env') === 'development';
        try {
            let errStatus = 500;
            const errRes: any = {
                code: 'undefined_error',
                message: 'Undefined error.'
            };

            if (err instanceof Error) {
                if (err instanceof HttpError) {
                    errStatus = err.status;
                    if (typeof err.code !== 'undefined') {
                        errRes.code = err.code;
                    }
                }

                if (isDebug) {
                    errRes.message = err.toString();
                    errRes.stack = err.stack;
                } else {
                    errRes.message = err.message;
                }
            } else {
                if (isDebug) {
                    errRes.message = err;
                }
            }

            return {errStatus, errRes};
        } catch (err2) {
            console.error('Error sending 500!', err2.stack);
        }
    }
}

const appServer = AppServer.getInstance();

export const server = appServer.server;
export const app = appServer.app;
