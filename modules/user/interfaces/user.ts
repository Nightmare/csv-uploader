'use strict';

import * as SequelizeStatic from 'sequelize';

export interface EmailSearchInterface<TInstance> {
    /**
     * Search for a single instance by its email. This applies LIMIT 1, so the listener will
     * always be called with a single instance.
     */
    findByEmail(email: string): Promise<TInstance | null>;
}

export interface UserAttribute {
    id?: string
    first_name?: string
    surname?: string
    email?: string
}

export interface UserInstance extends SequelizeStatic.Instance<UserAttribute>, UserAttribute {
}

export interface UserModel extends
    SequelizeStatic.Model<UserInstance, UserAttribute>,
    EmailSearchInterface<UserInstance> {
}
