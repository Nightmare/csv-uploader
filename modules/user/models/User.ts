'use strict';

import * as SequelizeStatic from 'sequelize';
import {DataTypes, Sequelize} from 'sequelize';
import {UserAttribute, UserInstance, UserModel} from '../interfaces/user';

export default function (sequelize: Sequelize, dataTypes: DataTypes): SequelizeStatic.Model<UserAttribute, UserInstance> {
    const User = sequelize.define<UserAttribute, UserInstance>('User', {
        id: {
            primaryKey: true,
            type: dataTypes.UUID,
            defaultValue: dataTypes.UUIDV4,
        },
        email: {
            type: dataTypes.STRING,
            allowNull: false,
            unique: {name: 'idx-users-email', msg: 'User with such email already exists.'},
            validate: {
                isEmail: true,
            },
        },
        first_name: {
            type: dataTypes.STRING,
            allowNull: false,
        },
        surname: {
            type: dataTypes.STRING,
            allowNull: false,
        },
    }, {
        tableName: 'users',
        freezeTableName: true,
        timestamps: false,
        underscored: true,
        classMethods: {
            /**
             * Returns user instance by email
             */
            findByEmail(email: string) {
                return User.findOne({where: {email}});
            },
        },
    }) as UserModel;

    return User;
}
