'use strict';

require('chai').should();

const path = require('path');
const request = require('supertest');
const {sequelize} = require('./../../dist/_shared/models');
const {app} = require('./../../dist/app');

describe('routes : cvs', () => {
    before(() => sequelize.sync({force: true}));

    describe('POST /cvs', () => {
        it('should parse CSV file correctly', () => {
            return request(app)
                .post('/csv')
                // todo: think about fake.js
                .attach('csv', path.resolve(__dirname, 'seeds', 'routes', 'csv', 'correct.csv'))
                .expect(200)
                .expect('Content-Type', /application\/json/)
                .expect({success: true})
                .then(() =>
                    request(app)
                        .get('/csv')
                        .expect(200)
                        .expect('Content-Type', /application\/json/)
                )
                .then(res => {
                    res.body.should.have.property('count');
                    res.body.should.have.property('users');
                    res.body.count.should.equal(12);
                    res.body.users.should.have.length(12);
                });
        });

        // todo: add negative scenarios
    });
});
