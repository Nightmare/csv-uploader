# How to...
* `npm i` - install the packages
* `npm run migrate` - create db and table(s)
* `npm run start:dev` - run project with debugging (dev mode, debug port is `5252`)
* `npm start` - run project
* `npm test` - run tests

# Open project
* [http://localhost:8081](http://localhost:8081)

# TODO
* add docker
* add CI
* add AWS ALB
* add istanbul for coverage code