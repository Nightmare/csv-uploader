'use strict';

export class ApplicationError extends Error {
    public parents: Error[];

    /**
     * @param {String=} message
     * @param {String|Number} [code]
     * @param {Error} [parent] The previous error used for the error chaining.
     */
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message);
        this.name = this.constructor.name;
        this.code = code || this.name;

        this.stack = (<any>new Error()).stack;
        this.expandParent();
    }

    expandParent() {
        let parentStack = [];

        if (this.parent instanceof Error) {
            parentStack.push({
                name: this.parent.name,
                message: this.parent.message,
                stack: this.parent.stack
            });
        }
        if (this.parent instanceof ApplicationError) {
            parentStack = parentStack.concat(this.parent.expandParent());
        }

        this.parents = parentStack;
        return parentStack;
    }
}

/**
 * HttpError represents an error caused by an improper request of the end-user.
 *
 * HttpError can be differentiated via its [[status]] property value which
 * keeps a standard HTTP status code (e.g. 404, 500). Error handlers may use this status code
 * to decide how to format the error page.
 *
 * Throwing an HttpError like in the following example will result in the 404 page to be displayed.
 *
 * ```js
 * if (item === null) { // item does not exist
 *     throw new HttpError(404, 'The requested Item could not be found.');
 * }
 * ```
 */
export class HttpError extends ApplicationError {
    /**
     * @param {Number} status
     * @param {String} message
     * @param {String|Number} [code]
     * @param {Error} [parent]
     */
    constructor(public status: number, message: string, code: number | string, parent?: Error) {
        super(message, code, parent);
    }
}

/**
 * NotFoundHttpError represents a "Not Found" HTTP error with status code 404.
 *
 * @link https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.5
 */
export class NotFoundHttpError extends HttpError {
    constructor(message: string, code?: string | number, parent?: Error) {
        super(404, message, code, parent);
    }
}
