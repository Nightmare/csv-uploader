'use strict';

import * as config from 'config';
import * as glob from 'glob';
import * as path from 'path';
import * as SequelizeStatic from 'sequelize';
import {Sequelize} from 'sequelize';
import {UserModel} from '../modules/user/interfaces/user';

export interface SequelizeModels {
    User: UserModel
}

class Database {
    private _models: SequelizeModels;
    private _sequelize: Sequelize;

    /**
     *
     * @param {object} dbConfig
     */
    constructor(private dbConfig) {
        this._sequelize = new SequelizeStatic(dbConfig);
        this._models = ({} as any);

        glob.sync(path.resolve(__dirname, '..', 'modules') + '/**/models/*.js')
            .filter((filepath: string) => {
                const basename = path.basename(filepath);
                return basename !== 'index.js' && basename.slice(-3) === '.js' && basename.indexOf('.') !== 0;
            })
            .forEach((filepath: string) => {
                const model = this._sequelize.import(filepath);
                this._models[(model as any).name] = model;
            });

        Object.keys(this._models).forEach((modelName: string) => {
            if (typeof this._models[modelName].associate === 'function') {
                this._models[modelName].associate(this._models);
            }
        });
    }

    /**
     * Returns models
     *
     * @returns {SequelizeModels}
     */
    getModels(): SequelizeModels {
        return this._models;
    }

    /**
     * Returns sequelize instance
     *
     * @returns {Sequelize}
     */
    getSequelize(): Sequelize {
        return this._sequelize;
    }
}

const database = new Database(config.get('sequelize'));
export const models = database.getModels();
export const sequelize = database.getSequelize();
