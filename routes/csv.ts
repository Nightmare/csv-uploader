'use strict';

import * as express from 'express';
import * as Busboy from 'busboy';
import * as debug from 'debug';
import * as Joi from 'joi';
import {models} from '../_shared/models';
const csv = require('fast-csv');

const log = debug('CSV:');
const error = debug('CSV:');
log.log = console.log.bind(console);

export const router = express.Router();

router.get('/', async (req: express.Request, res: express.Response) => {
    //todo: add querying, paging etc.
    const  {rows: users, count} = await models.User.findAndCountAll();
    return res.json({users, count});
});

router.post('/', async (req: express.Request, res: express.Response) => {
    const busboy = new Busboy({headers: req.headers});
    busboy
        .on('file', (field, file) => {
            if (field !== 'csv') {
                return log('SKIP parsing file with field name %s', field);
            }
            let csvData = [];

            file.pipe(csv())
                .on('data', async (data: string[]) => {
                    if (!Array.isArray(data) || data.length !== 3) {
                        return log('SKIPPING INSERTING CSV DATASET - INVALID DATASET');
                    }

                    try {
                        const [first_name, surname, email] = data;
                        const objectData = {first_name, surname, email};

                        Joi.attempt(objectData, Joi.object().keys({
                            email: Joi.string().email(),
                            first_name: Joi.string().max(255),
                            surname: Joi.string().max(255),
                        }).options({stripUnknown: true, presence: 'required'}));

                        csvData.push(objectData);
                        if (csvData.length >= 1000) {
                            await models.User.bulkCreate(csvData);
                            csvData = [];
                        }
                    } catch (err) {
                        error('ERROR VALIDATING CSV DATA SET:', err);
                    }
                })
                .on('end', async () => {
                    if (csvData.length > 0) {
                        await models.User.bulkCreate(csvData);
                    }
                });
        })
        .on('finish', () => res.json({success: true}));
    req.pipe(busboy);
});